# Simple script(s) using the python version of the Robotics Toolbox

The robotics Toolbox is originally [a matlab toolbox developped by Peter Corke](https://petercorke.com/toolboxes/robotics-toolbox/) mostly dedicated to robot modeling which is also availbale as [github project](https://github.com/petercorke/robotics-toolbox-matlab).

The [Python version](https://github.com/petercorke/robotics-toolbox-matlab) may not be as mature but it provides a way of accessing the modeling tools of the toolbox without the need of using matlab.

The installation procedure of this toolbox is provided by the Readme.md of the github project and the fastest way to get it running is to use the latest release version which can be installed in the following way:

`pip3 install roboticstoolbox-python`

You may also need to install [sympy](https://github.com/sympy/sympy)

`pip3 install sympy`

(for more install options see [this](https://github.com/petercorke/robotics-toolbox-python#installing))

However, before you do so, I would suggest first to install [Anaconda](https://www.anaconda.com/products/individual) which allows you to create clean python environments living together in harmony in the same OS. This avoids mixing up various versions of Python and Python libraries but also provides many preinstalled scientific libraries.

Once Anaconda is installed, you can follow [this tutorial](https://conda.io/projects/conda/en/latest/user-guide/getting-started.html) to understand how to create a specific conda environment.

As for a Python IDE, I would suggest the use of [Visual Studio Code](https://code.visualstudio.com/) which is both a good Python editor (with completion), a decent IDE and a tool which works well with conda.

In summary, after having installed Anaconda:

`conda create --name rtb python=3.8`

`conda activate rtb`

`conda install -c anaconda sympy` or
`pip3 install sympy`

`pip3 install roboticstoolbox-python`

## Setting up a Conda Env

This *should* be all the necessary steps for an isolated minimalist environment:

```bash
conda create -c conda-forge --name rtb roboticstoolbox-python # creates the environment
conda activate rtb
```

To check the environment was created: `conda env list`

