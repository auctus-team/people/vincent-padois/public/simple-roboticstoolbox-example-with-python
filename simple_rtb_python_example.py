#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 21:53:31 2021

@author: vpadois
"""

import roboticstoolbox as rtb
from spatialmath import SE3

rob = rtb.models.DH.Panda()         # create an instance of Panda robot
#print(rob)

T = rob.fkine(rob.qz)  # forward kinematics
print(T)

T2 = SE3(0.5, 0.3, 0.4) * SE3.OA([0, 1, 0], [0, 0, -1])     # target pose

sol = rob.ikine_LM(T2)           # solve IK
print(sol[0])                 # display solution

qt = rtb.tools.trajectory.jtraj(rob.qz, sol[0], 50)
#rob.plot(qt.q, movie='panda1.gif')

robviz = rtb.models.URDF.Panda()    # another way of creating a robot
env = rtb.backends.Swift()          # instantiate 3D browser-based visualizer
env.launch()                        # activate it
env.add(robviz)                     # add robot to the 3D scene

while True:
      for qk in qt.q:             # for each joint configuration on trajectory
            robviz.q = qk         # update the robot state
            env.step()            # update visualization

      for qk in reversed(qt.q):   # Go back           
            robviz.q = qk         
            env.step()   
